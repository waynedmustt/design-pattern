import { Motor } from './Motor';
import { Observable, of } from 'rxjs';

export class Bike extends Motor {
    constructor() {
        super();
        this.type = 'Bike';
        this.wheelsAmount = 2;
    }

    getData(): Observable<any> {
        return of({
           success: true,
           data: [
               {
                  brand: 'Polygon',
                  wheelsAmount: this.wheelsAmount
               }
           ]
        });
    }
}
