import { Vehicle } from './Vehicle';
import { Observable, of } from 'rxjs';

export class Car extends Vehicle {
    public windowsType;

    constructor() {
        super();
        this.type = 'Car';
        this.wheelsAmount = 4;
    }

    getWindowType() {
        return this.windowsType;
    }

    setWindowType(windowType) {
        this.windowsType = windowType;
    }

    getData(): Observable<any> {
        return of({
            success: true,
            data: [
                {
                    brand: 'Mercedes-Benz',
                    wheelsAmount: this.wheelsAmount
                },
                {
                    brand: 'BMW',
                    wheelsAmount: this.wheelsAmount
                },
                {
                    brand: 'Toyota',
                    wheelsAmount: this.wheelsAmount
                }
            ]
        });
    }
}
