import { Vehicle } from './Vehicle';
import { Observable, of } from 'rxjs';

export class Motor extends Vehicle {
    public luggageSize;

    constructor() {
        super();
        this.type = 'Motor';
        this.wheelsAmount = 2;
    }

    getLuggageSize() {
        return this.luggageSize;
    }

    setLuggageSize(luggageSize) {
        this.luggageSize = luggageSize;
    }

    getData(): Observable<any> {
        return of({
           success: true,
           data: [
               {
                  brand: 'Yamaha',
                  wheelsAmount: this.wheelsAmount
               },
               {
                  brand: 'Honda',
                  wheelsAmount: this.wheelsAmount
               }
           ]
        });
    }
}
