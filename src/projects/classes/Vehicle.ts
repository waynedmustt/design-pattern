export class Vehicle {
    public type: string;
    public wheelsAmount: any;
    public brand: string;

    getType() {
        return this.type;
    }

    setType(type) {
        this.type = type;
    }

    getWheelsAmount() {
        return this.wheelsAmount;
    }

    setWheelsAmount(wheelsAmount) {
        this.wheelsAmount = wheelsAmount;
    }

    getBrand() {
        return this.brand;
    }

    setBrand(brand) {
        this.brand = brand;
    }
}
