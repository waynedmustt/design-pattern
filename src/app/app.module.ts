import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { Vehicle } from '../projects/classes/Vehicle';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule
  ],
  providers: [
    Vehicle
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
