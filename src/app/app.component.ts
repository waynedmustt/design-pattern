import { Component, OnInit } from '@angular/core';
import { Car } from '../projects/classes/Car';
import { Motor } from '../projects/classes/Motor';
import { Bike } from '../projects/classes/Bike';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title: any;
  public vehicles: any;
  public vechileLists: any = [
    {
      value: 'car',
      label: 'Car'
    },
    {
      value: 'motor',
      label: 'Motor'
    },
    {
      value: 'bike',
      label: 'Bike'
    }
  ];
  public selectedVechile: any;

  constructor(
  ) {
  }

  ngOnInit() {
    this.title = this._setTitle();
    this.selectedVechile = this.vechileLists[0];
    this.getVehicle(this.selectedVechile.value);
  }

  private _setTitle() {
    return 'design-pattern';
  }

  selectVechile(selectedVechile) {
    if (!selectedVechile) {
      return;
    }

    this.getVehicle(selectedVechile.value);
  }

  getVehicle(type) {
    let dataSource: any;
    switch(type) {
      case 'car':
        dataSource = new Car();
        break;
      case 'motor':
        dataSource = new Motor();
        break;
      case 'bike':
        dataSource = new Bike();
        break;
      default:
        dataSource = null;
        break;
    }

    if (!dataSource) {
      this.vehicles = [];
      return;
    }

    dataSource.getData().subscribe((response) => {
      if (!response.success || !response.data) {
        return [];
      }

      this.vehicles = response.data;
    });
  }
}
